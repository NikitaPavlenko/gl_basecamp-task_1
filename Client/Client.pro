QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    screenshot_client.cpp

HEADERS += \
    mainwindow.h \
    screenshot_client.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../ScreenshotWorker/release/ -lScreenshotWorker
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../ScreenshotWorker/debug/ -lScreenshotWorker
else:unix: LIBS += -L$$OUT_PWD/../ScreenshotWorker/ -lScreenshotWorker

INCLUDEPATH += $$PWD/../ScreenshotWorker
DEPENDPATH += $$PWD/../ScreenshotWorker

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ScreenshotWorker/release/libScreenshotWorker.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ScreenshotWorker/debug/libScreenshotWorker.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ScreenshotWorker/release/ScreenshotWorker.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ScreenshotWorker/debug/ScreenshotWorker.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../ScreenshotWorker/libScreenshotWorker.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Net/release/ -lNet
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Net/debug/ -lNet
else:unix: LIBS += -L$$OUT_PWD/../Net/ -lNet

INCLUDEPATH += $$PWD/../Net
DEPENDPATH += $$PWD/../Net

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Net/release/libNet.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Net/debug/libNet.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Net/release/Net.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Net/debug/Net.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Net/libNet.a
