#pragma once

#include <client_interface.h>
#include <QTimer>
#include <screenshot_worker.h>
#include <QStringList>

enum ScreenshotMsgTypes : uint16_t
{
	kScreenshotNameList = 0,
	kScreenshotImage,
	kNewScreenshotName
};

class ScreenshotClient
		: public net::ClientInterface
{
		Q_OBJECT
	public:
		ScreenshotClient(const QString& screenshot_directory, uint32_t msec_print_screen_interval,
							  const char* screenshot_format, QObject* parent = nullptr);
		virtual ~ScreenshotClient() override;

		void SendScreenshotNameList(const QStringList& screenshot_name_list);
		void SendScreenshotImage(const QImage& screenshot);
		void SendNewScreenshotName(const QString& screenshot_name);

		// ClientInterface interface
	private slots:
		void slot_Connected() override;
		void slot_Disconnected() override;
		//Parses incoming message
		void slot_ParseMessage(net::Message& Msg) override;
		//Called when we need to make screenshot
		void slot_TimeOut();

	private:
		QStringList GetScreenshotList();

		QTimer print_screen_timer_;
		QString screenshot_directory_;
		const char* screenshot_format_;
		IWorker<QImage>* screenshot_worker_;
};
