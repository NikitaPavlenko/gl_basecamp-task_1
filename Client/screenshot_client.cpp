#include "screenshot_client.h"

#include <reader_image_in_directory.h>
#include <saver_image_by_date.h>
#include <maker_print_screen.h>
#include <QDirIterator>
#include <QCollator>

ScreenshotClient::ScreenshotClient(const QString& screenshot_directory,
																	 uint32_t msec_print_screen_interval,
																	 const char* screenshot_format, QObject* parent)
	: ClientInterface(parent)
	, screenshot_directory_(screenshot_directory)
	, screenshot_format_(screenshot_format)
	, screenshot_worker_(new ScreenshotWorker(new ReaderImageInDirectory(screenshot_directory_),
																						new SaverImageByDate(screenshot_directory_,
																																 screenshot_format_),
																						new MakerPrintScreen()))
{
	connect(&print_screen_timer_, &QTimer::timeout, this, &ScreenshotClient::slot_TimeOut);

	print_screen_timer_.setInterval(msec_print_screen_interval);
	print_screen_timer_.start();
}

ScreenshotClient::~ScreenshotClient()
{
	delete screenshot_worker_;
}

void ScreenshotClient::SendScreenshotNameList(const QStringList& screenshot_name_list)
{
	net::Message msg;
	msg.header.type = ScreenshotMsgTypes::kScreenshotNameList;

	if (screenshot_name_list.count() != 0)
	{
		for (const QString& screenshot: screenshot_name_list)
			msg << screenshot.toStdString();

		msg << screenshot_name_list.count();
	}
	SendMsgToServer(msg);
}

void ScreenshotClient::SendScreenshotImage(const QImage& screenshot_image)
{
	net::Message msg;
	msg.header.type = ScreenshotMsgTypes::kScreenshotImage;

	QByteArray byte_buffer;
	QDataStream streamOut(&byte_buffer, QIODevice::WriteOnly);

	streamOut << screenshot_image;
	msg << byte_buffer.toStdString();

	SendMsgToServer(msg);
}

void ScreenshotClient::SendNewScreenshotName(const QString& screenshot_name)
{
	net::Message msg;
	msg.header.type = ScreenshotMsgTypes::kNewScreenshotName;

	msg << screenshot_name.toStdString();
	SendMsgToServer(msg);
}

void ScreenshotClient::slot_ParseMessage(net::Message& msg)
{
	switch(msg.header.type)
	{
		case ScreenshotMsgTypes::kScreenshotNameList:
		{
			SendScreenshotNameList(GetScreenshotList());
			break;
		}
		case ScreenshotMsgTypes::kScreenshotImage:
		{
			std::string screenshot_name;
			msg >> screenshot_name;

			SendScreenshotImage(screenshot_worker_->Read(QString::fromStdString(screenshot_name)));
			break;
		}
		default:
			qCritical() << "Unknown meessage";
			break;
	}
}

void ScreenshotClient::slot_TimeOut()
{
	SendNewScreenshotName(screenshot_worker_->Write(screenshot_worker_->Make()));
}

QStringList ScreenshotClient::GetScreenshotList()
{
	QDirIterator screenshot_iterator(QString(screenshot_directory_),
																	 QStringList("*." + QString(screenshot_format_)),
																	 QDir::Files);
	QStringList screenshot_list;
	while (screenshot_iterator.hasNext())
	{
		screenshot_iterator.next();
		screenshot_list << screenshot_iterator.fileName();
	}
	QCollator collator;
	collator.setNumericMode(true);
	std::sort(screenshot_list.begin(),screenshot_list.end(),
						[&collator](const QString &file1, const QString &file2)
	{
		return collator.compare(file1, file2) < 0;
	});
	return screenshot_list;
}

void ScreenshotClient::slot_Connected() { }

void ScreenshotClient::slot_Disconnected() { }
