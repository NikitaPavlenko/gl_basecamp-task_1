#pragma once

#include <QMainWindow>
#include "screenshot_client.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow
		: public QMainWindow
{
		Q_OBJECT
	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();

	private slots:
		void slot_CheckConnection();

	private:
		Ui::MainWindow *ui;
		QTimer connection_checker_timer_;
		QString ip_address_string_;
		uint32_t msec_print_screen_interval_;
		uint32_t msec_reconnect_interval_;
		uint16_t port_;
		ScreenshotClient client_;
};
