#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, ip_address_string_("localhost")
	, msec_print_screen_interval_(60000)
	, msec_reconnect_interval_(3600000)
	, port_(5555)
	, client_(QCoreApplication::applicationDirPath() + "/" + "Screenshots",
				 msec_print_screen_interval_, "png")
{
	ui->setupUi(this);

	connect(&connection_checker_timer_, &QTimer::timeout, this, &MainWindow::slot_CheckConnection);

	connection_checker_timer_.setInterval(msec_reconnect_interval_);

	client_.Connect(ip_address_string_, port_);
	connection_checker_timer_.start();
}

void MainWindow::slot_CheckConnection()
{
	if (!client_.IsConnected())
		client_.Connect(ip_address_string_, port_);
}

MainWindow::~MainWindow()
{
	if (client_.IsConnected())
		client_.Disconnect();
	delete ui;
}
