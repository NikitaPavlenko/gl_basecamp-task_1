#pragma once

#include <QMainWindow>

class ScreenshotServer;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow
		: public QMainWindow
{
		Q_OBJECT
	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();

	private slots:
		void on_request_button_clicked();

	private:
		void resizeEvent(QResizeEvent *event) override;

		Ui::MainWindow* ui;
		ScreenshotServer* server_;

};
