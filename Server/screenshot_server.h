#pragma once

#include <server_interface.h>
#include <QGraphicsScene>
#include <QTreeWidgetItem>

enum ScreenshotMsgTypes : uint16_t
{
	kScreenshotNameList = 0,
	kScreenshotImage,
	kNewScreenshotName
};

class QGraphicsScene;

class ScreenshotServer
		: public net::ServerInterface
{
		struct ClientData
		{
				net::Connection* connection;
				QTreeWidgetItem* tree_root_item;
		};
		Q_OBJECT
	public:
		ScreenshotServer(uint16_t port, QObject* parent = nullptr);

		void RequestScreenshotNameList(net::Connection* connection);
		void RequestScreenshotImage(net::Connection* connection, const QString& screenshot_name);

		QGraphicsScene* GetScene();
		void SetTreeWidget(QTreeWidget* tree_widget);

		net::Connection* GetConnection(QTreeWidgetItem* tree_item);

		// ServerInterface interface
	private slots:
		void slot_ParseMessage(net::Message& msg, net::Connection* sender_connection) override;

	private:
		void ClientConnected(net::Connection* connection) override;
		void ClientDisconnected(net::Connection* connection) override;

		void SetScreenshotImage(QImage& screenshot_image);
		void SetScreenshotNameList(const QStringList& screenshot_name, net::Connection* connection);
		void AddNewScreenshotName(const QString& screenshot_name, net::Connection* connection);

		QString GetConnectionName(net::Connection* connection);

		std::map<qintptr, ClientData> connection_list_;
		QTreeWidget* screenshot_tree_by_client_;
		QGraphicsScene screenshot_scene_;
		QGraphicsPixmapItem* screenshot_pixmap_;
};

