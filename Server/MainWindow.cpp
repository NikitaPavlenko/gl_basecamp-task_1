#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "screenshot_server.h"
#include <QPropertyAnimation>
#include <QMouseEvent>
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, server_(new ScreenshotServer(5555, this))
{
	ui->setupUi(this);

	ui->screenshot_tree->setHeaderLabels({"Address", "Screenshot name", "Id"});
	ui->screenshot_tree->header()->setDefaultAlignment(Qt::AlignCenter);
	ui->screenshot_tree->header()->setHighlightSections(true);
	ui->screenshot_tree->setColumnWidth(0, 140);
	ui->screenshot_tree->setColumnWidth(1, 115);
	ui->screenshot_tree->header()->setSectionHidden(2, true);
	ui->screenshot_tree->setSelectionBehavior(QAbstractItemView::SelectItems);

	ui->screenshot_view->setScene(server_->GetScene());

	server_->SetTreeWidget(ui->screenshot_tree);
	server_->Start();
}

MainWindow::~MainWindow()
{
	delete server_;
	delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
	QMainWindow::resizeEvent(event);

	ui->screenshot_view->fitInView(ui->screenshot_view->scene()->sceneRect(),Qt::KeepAspectRatio);
}

void MainWindow::on_request_button_clicked()
{
	if (ui->screenshot_tree->currentColumn() == 1)
	{
		QTreeWidgetItem* selected_tree_item = ui->screenshot_tree->currentItem();
		server_->RequestScreenshotImage(server_->GetConnection(selected_tree_item->parent()),
																		selected_tree_item->text(1));
	}
}
