#include "screenshot_server.h"
#include <QImage>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

ScreenshotServer::ScreenshotServer(uint16_t port, QObject* parent)
	: ServerInterface(port, parent)
{ }

void ScreenshotServer::RequestScreenshotNameList(net::Connection* connection)
{
	net::Message new_message;
	new_message.header.type = ScreenshotMsgTypes::kScreenshotNameList;

	SendMessageToClient(new_message, connection);
}

void ScreenshotServer::RequestScreenshotImage(net::Connection* connection, const QString& screenshot_name)
{
	net::Message new_message;
	new_message.header.type = ScreenshotMsgTypes::kScreenshotImage;
	new_message << screenshot_name.toStdString();

	SendMessageToClient(new_message, connection);
}

QGraphicsScene* ScreenshotServer::GetScene()
{
	return &screenshot_scene_;
}

void ScreenshotServer::SetTreeWidget(QTreeWidget* tree_widget)
{
	screenshot_tree_by_client_ = tree_widget;
}

net::Connection* ScreenshotServer::GetConnection(QTreeWidgetItem* tree_item)
{
	return connection_list_[tree_item->text(2).toInt()].connection;
}

void ScreenshotServer::slot_ParseMessage(net::Message& msg, net::Connection* sender_connection)
{
	switch(msg.header.type)
	{
		case ScreenshotMsgTypes::kScreenshotNameList:
		{
			if (msg.header.size == 0)
				return;

			qsizetype screenshots_count;
			msg >> screenshots_count;

			QStringList screenshot_list(screenshots_count);

			for (int i = 0; i < screenshots_count; i++)
			{
				std::string screenshot_name;
				msg >> screenshot_name;
				screenshot_list[i] = QString::fromStdString(screenshot_name);
			}
			SetScreenshotNameList(screenshot_list, sender_connection);
			break;
		}
		case ScreenshotMsgTypes::kScreenshotImage:
		{
			std::string string_buffer;
			msg >> string_buffer;

			QByteArray byte_buffer = QByteArray::fromStdString(string_buffer);
			QDataStream streamIn(&byte_buffer, QIODevice::ReadOnly);

			QImage screenshot_image;
			streamIn >> screenshot_image;

			SetScreenshotImage(screenshot_image);
			break;
		}
		case ScreenshotMsgTypes::kNewScreenshotName:
		{
			std::string string_buffer;
			msg >> string_buffer;

			AddNewScreenshotName(QString::fromStdString(string_buffer), sender_connection);
			break;
		}
		default:
		{
			qCritical() << "Unknown meessage";
			break;
		}
	}
}

void ScreenshotServer::ClientConnected(net::Connection* connection)
{	
	qintptr id_connection = connection->Id();
	QTreeWidgetItem* root_item = new QTreeWidgetItem(screenshot_tree_by_client_);
	root_item->setText(2, QString::number(id_connection));
	root_item->setText(0, GetConnectionName(connection));

	connection_list_[connection->Id()] = ClientData{connection, root_item};

	RequestScreenshotNameList(connection);
}

void ScreenshotServer::ClientDisconnected(net::Connection* connection)
{	
	quint32 id_connection = connection->Id();
	QTreeWidgetItem* root_item = connection_list_[id_connection].tree_root_item;

	QList<QTreeWidgetItem*> child_item_list = root_item->takeChildren();
	if (child_item_list.count() != 0)
		for (auto child_item: child_item_list)
			delete child_item;

	screenshot_tree_by_client_->removeItemWidget(root_item, 0);
	connection_list_.erase(id_connection);

	delete root_item;
}

void ScreenshotServer::SetScreenshotImage(QImage& screenshot_image)
{
	QGraphicsView* screenshot_view = screenshot_scene_.views()[0];
	if (screenshot_view)
	{
		if (screenshot_pixmap_ == nullptr)
			screenshot_pixmap_ = screenshot_scene_.addPixmap(QPixmap::fromImage(screenshot_image));
		else
			screenshot_pixmap_->setPixmap(QPixmap::fromImage(screenshot_image));
		screenshot_view->fitInView(screenshot_scene_.sceneRect(),Qt::KeepAspectRatio);
	}
}

void ScreenshotServer::SetScreenshotNameList(const QStringList& screenshot_name_list, net::Connection* connection)
{
	QTreeWidgetItem* tree_item = connection_list_[connection->Id()].tree_root_item;

	auto it = screenshot_name_list.cbegin();
	for (;it != screenshot_name_list.cend(); ++it)
	{
		QTreeWidgetItem *child_item = new QTreeWidgetItem();
		child_item->setText(1, *it);
		tree_item->addChild(child_item);
	}
}

void ScreenshotServer::AddNewScreenshotName(const QString& screenshot_name, net::Connection* connection)
{
	qintptr id_connection = connection->Id();
	QTreeWidgetItem* child_tree_item = new QTreeWidgetItem();
	child_tree_item->setText(1, screenshot_name);
	connection_list_[id_connection].tree_root_item->insertChild(0, child_tree_item);
}

QString ScreenshotServer::GetConnectionName(net::Connection* connection)
{
	return connection->peerAddress().toString() + ":" + QString::number(connection->peerPort());
}
