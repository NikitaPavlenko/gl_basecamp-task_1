#pragma once

#include <vector>
#include <cstdint>
#include <iostream>
#include <cstring>
#include <QDataStream>
#include <QByteArray>


namespace net
{
struct MessageHeader
{
		int16_t type;
		uint32_t size;
};

struct Message
{
		MessageHeader header{};
		std::vector<uint8_t> body{};

		uint32_t Size()
		{
			return sizeof(MessageHeader) + body.size();
		}

		friend QDataStream& operator << (QDataStream& out_stream, const Message& out_msg)
		{
			out_stream.writeRawData(reinterpret_cast<const char*>(&out_msg.header), sizeof(MessageHeader));
			out_stream.writeRawData(reinterpret_cast<const char*>(out_msg.body.data()), out_msg.header.size);

			return out_stream;
		}

		friend QDataStream& operator >> (QDataStream& in_stream, Message& in_msg)
		{
			in_stream.readRawData(reinterpret_cast<char*>(&in_msg.header), sizeof(MessageHeader));
			in_msg.body.resize(in_msg.header.size);
			in_stream.readRawData(reinterpret_cast<char*>(in_msg.body.data()), in_msg.header.size);

			return in_stream;
		}

		template <typename DataType>
		friend Message& operator << (Message& msg, const DataType& data)
		{
			static_assert (std::is_standard_layout<DataType>::value, "Data is too complex to be pushed into vector");

			size_t i = msg.body.size();
			msg.body.resize(msg.body.size() + sizeof(DataType));
			std::memcpy(msg.body.data() + i, &data, sizeof(DataType));
			msg.header.size = msg.body.size();

			return msg;
		}

		template <typename DataType>
		friend Message& operator >> (Message& msg, DataType& data)
		{
			static_assert(std::is_standard_layout<DataType>::value, "Data is too complex to be pushed from vector");
			size_t i = msg.body.size() - sizeof(DataType);
			std::memcpy(&data, msg.body.data() + i, sizeof(DataType));
			msg.body.resize(i);
			msg.header.size = msg.body.size();

			return msg;
		}

		//		friend Message& operator << (Message& msg, const std::string& data)
		//		{
		//			size_t offset_index = msg.body.size();
		//			size_t data_size = data.size();

		//			msg.body.resize(msg.body.size() + sizeof(data[0]) * data_size + sizeof(size_t));
		//			std::memcpy(msg.body.data() + offset_index, data.data(), sizeof(data[0]) * data_size);
		//			offset_index += sizeof(data[0]) * data_size;
		//			std::memcpy(msg.body.data() + offset_index, &data_size, sizeof(size_t));
		//			msg.header.size = msg.body.size();

		//			return msg;
		//		}

		//		friend Message& operator >> (Message& msg, std::string& data)
		//		{
		//			size_t offset_index = msg.body.size() - sizeof(size_t);
		//			size_t data_size = 0;

		//			std::memcpy(&data_size, msg.body.data() + offset_index, sizeof(size_t));
		//			data.resize(data_size);
		//			msg.body.resize(offset_index);
		//			offset_index = msg.body.size() - sizeof(data[0]) * data_size;
		//			std::memcpy(&data[0], msg.body.data() + offset_index, sizeof(data[0]) * data_size);
		//			msg.body.resize(offset_index);
		//			msg.header.size = msg.body.size();

		//			return msg;
		//		}

		friend Message& operator << (Message& msg, const std::string& data)
		{
			size_t i = msg.body.size();
			size_t nDataSize = data.size();

			msg.body.resize(msg.body.size() + sizeof(data[0]) * nDataSize + sizeof(size_t));

			std::memcpy(msg.body.data() + i, data.data(), sizeof(data[0]) * nDataSize);
			i += sizeof(data[0]) * nDataSize;

			std::memcpy(msg.body.data() + i, &nDataSize, sizeof(size_t));

			msg.header.size = msg.body.size();

			return msg;
		}

		friend Message& operator >> (Message& msg, std::string& data)
		{
			size_t nDataSize = 0;
			size_t i = msg.body.size() - sizeof(size_t);

			std::memcpy(&nDataSize, msg.body.data() + i, sizeof(size_t));

			data.resize(nDataSize);

			msg.body.resize(i);

			i = msg.body.size() - sizeof(data[0]) * nDataSize;

			std::memcpy(&data[0], msg.body.data() + i, sizeof(data[0]) * nDataSize);

			msg.body.resize(i);

			msg.header.size = msg.body.size();

			return msg;
		}
};
}
