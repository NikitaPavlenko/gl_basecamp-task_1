#pragma once

#include <QTcpSocket>
#include <QScopedPointer>
#include "message.h"

namespace net {

class Connection
		: public QTcpSocket
{
		Q_OBJECT
	public:
		Connection(QObject* parent = nullptr);
		Connection(qintptr socket_descriptor, quint32 id, QObject *parent = nullptr);

		void SendMessage(const Message& msg);
		bool IsConnected();
		bool Disconnect();

		quint32 Id() const { return id_; }

	signals:
		void signal_NewMessage(Message& msg, Connection* connection = nullptr);

	private slots:
		void slot_ReadyRead();

	private:
		Message temporary_msg_;
		quint32 id_;
};

} // namespace net
