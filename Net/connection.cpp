#include "connection.h"
#include "message.h"

namespace net {

Connection::Connection(QObject* parent)
	: QTcpSocket(parent)
	, id_(0)
{
	connect(this, &Connection::readyRead, this, &Connection::slot_ReadyRead);
}

Connection::Connection(qintptr socket_descriptor, quint32 id, QObject* parent)
	: QTcpSocket(parent)
	, id_(id)
{
	setSocketDescriptor(socket_descriptor);
	connect(this, &Connection::readyRead, this, &Connection::slot_ReadyRead);
}

void Connection::SendMessage(const Message& msg)
{
	QByteArray  buffer;
	QDataStream out_stream(&buffer, QIODevice::WriteOnly);

	out_stream << msg;

	this->write(buffer);
}

bool Connection::IsConnected()
{
	return this->state() == QTcpSocket::ConnectedState;
}

bool Connection::Disconnect()
{
	disconnectFromHost();
	if (state() == QAbstractSocket::UnconnectedState || waitForDisconnected(1000))
		return true;
	return false;
}

void Connection::slot_ReadyRead()
{
	QDataStream input_stream(this);
	while (true)
	{
		if (temporary_msg_.header.size == 0)
		{
			if (this->bytesAvailable() < static_cast<qint64>(sizeof(MessageHeader)))
				break;

			input_stream.readRawData(reinterpret_cast<char*>(&temporary_msg_.header), sizeof(MessageHeader));

			if (temporary_msg_.header.size == 0)
			{
				emit signal_NewMessage(temporary_msg_, this);
				break;
			}
		}

		if (this->bytesAvailable() < (qint64)temporary_msg_.header.size)
			break;

		temporary_msg_.body.resize(temporary_msg_.header.size);
		input_stream.readRawData(reinterpret_cast<char*>(temporary_msg_.body.data()), temporary_msg_.header.size);

		emit signal_NewMessage(temporary_msg_, this);

		temporary_msg_.header.size = 0;
		if (!temporary_msg_.body.size())
			temporary_msg_.body.resize(0);
	}
}
} // namespace net
