#include "client_interface.h"

namespace net {

ClientInterface::ClientInterface(QObject *parent)
	: QObject(parent)
{
	connection_ = new Connection(this);

	connect(connection_, &Connection::connected, this, &ClientInterface::slot_Connected);
	connect(connection_, &Connection::signal_NewMessage, this, &ClientInterface::slot_ParseMessage);
	connect(connection_, &Connection::disconnected, this, &ClientInterface::slot_Disconnected);
}

ClientInterface::~ClientInterface()
{
	delete connection_;
}

bool ClientInterface::Connect(const QString& host, uint16_t port)
{
	connection_->connectToHost(host, port);
	if (connection_->waitForConnected(1000))
		return true;
	return false;
}

bool ClientInterface::Disconnect()
{
	return connection_->Disconnect();
}

bool ClientInterface::IsConnected()
{
	return connection_->IsConnected();
}

void ClientInterface::SendMsgToServer(const Message& msg)
{
	connection_->SendMessage(msg);
}

} // namespace net
