#include "server_interface.h"
#include "connection.h"
#include <QTcpSocket>

namespace net {

ServerInterface::ServerInterface(quint16 port, QObject* parent)
	: QTcpServer(parent)
	, id_counter_(1000)
	, port_(port)
{

}

bool ServerInterface::Start()
{
	if (!this->listen(QHostAddress::AnyIPv4, port_))
		return false;
	return true;
}

void ServerInterface::Stop()
{
	if (this->isListening())
		this->close();
}

void ServerInterface::SendMessageToClient(const Message& msg, Connection* connection)
{
	if (connection && connection->IsConnected())
		connection->SendMessage(msg);
}

void ServerInterface::incomingConnection(qintptr handle)
{	
	Connection* connection = new Connection(handle, id_counter_++, this);

	connect(connection, &Connection::disconnected, this, &ServerInterface::slot_Disconnected);
	connect(connection, &Connection::signal_NewMessage, this, &ServerInterface::slot_ParseMessage);

	ClientConnected(connection);
}

void ServerInterface::slot_Disconnected()
{
	Connection* connection = dynamic_cast<Connection*>(sender());

	ClientDisconnected(connection);

	delete connection;
	connection = nullptr;
}

} // namespace net
