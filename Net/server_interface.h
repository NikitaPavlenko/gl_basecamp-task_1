#pragma once

#include <QTcpServer>
#include "connection.h"
#include "message.h"

namespace net {

class ServerInterface
		: public QTcpServer
{
	public:
		ServerInterface(quint16 port, QObject* parent = nullptr);

	public:
		bool Start();
		void Stop();

		void SendMessageToClient(const Message& msg, Connection* connection);

	protected:
		//Called when client is connected
		virtual void ClientConnected(Connection* connection) = 0;
		//Called when client is disconnected
		virtual void ClientDisconnected(Connection* connection) = 0;

	private slots:
		//Called when client was disconnected
		void slot_Disconnected();
		//Parses a message from client
		virtual void slot_ParseMessage(Message& msg, Connection* connection) = 0;

	private:
		//Called when new client connected
		void incomingConnection(qintptr handle) override;

		quint32 id_counter_;
		quint16 port_;
};

} // namespace net

