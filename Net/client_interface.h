#pragma once

#include <QObject>
#include "message.h"
#include "connection.h"

namespace net {

class ClientInterface
		: public QObject
{
		Q_OBJECT
	public:
		explicit ClientInterface(QObject *parent = nullptr);
		virtual ~ClientInterface();

		bool Connect(const QString& host, uint16_t port);
		bool Disconnect();
		bool IsConnected();

		void SendMsgToServer(const Message& Msg);

	protected slots:
		//Calls when socket connected to server
		virtual void slot_Connected() = 0;
		//Calls when socket disconnected from server
		virtual void slot_Disconnected() = 0;
		//Parses a message from client
		virtual void slot_ParseMessage(Message& Msg) = 0;

	protected:
		Connection* connection_;
};

} // namespace net
