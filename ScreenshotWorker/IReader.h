#pragma once
#include <QString>

template <typename T>
class IReader
{
public:
	virtual ~IReader() {}
	virtual T Read(const QString&) = 0;
};
