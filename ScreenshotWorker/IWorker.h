#pragma once
#include "ireader.h"
#include "iwriter.h"
#include "imaker.h"

template <typename T>
class IWorker
		: public IReader<T>
		, public IWriter<T>
		, public IMaker<T>
{
};
