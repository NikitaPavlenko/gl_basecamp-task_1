#include "reader_image_in_directory.h"

ReaderImageInDirectory::ReaderImageInDirectory(const QString& image_directory)
	: image_directory_(image_directory)
{ }

QImage ReaderImageInDirectory::Read(const QString& image_name)
{
	QImage image;
	if (image.load(image_directory_ + "/" + image_name))
		return image;
	else
		return QImage{};
}
