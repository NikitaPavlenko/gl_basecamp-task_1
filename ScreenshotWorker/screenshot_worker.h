#pragma once

#include "iworker.h"
#include <QImage>

class ScreenshotWorker
		: public IWorker<QImage>
{
public:
	ScreenshotWorker(IReader<QImage>* pReader, IWriter<QImage>* pWriter, IMaker<QImage>* pMaker)
		: m_pReader(pReader)
		, m_pWriter(pWriter)
		, m_pMaker(pMaker)
	{ }

	virtual ~ScreenshotWorker()
	{
		delete m_pReader;
		delete m_pWriter;
		delete m_pMaker;
	}

	// IMaker interface
public:
	QImage Make() override { return m_pMaker->Make(); }

	// IWriter interface
public:
	QString Write(const QImage& data) override { return m_pWriter->Write(data); }

	// IReader interface
public:
	QImage Read(const QString& image_name) override { return m_pReader->Read(image_name); }

private:
	IReader<QImage>* m_pReader;
	IWriter<QImage>* m_pWriter;
	IMaker<QImage>* m_pMaker;
};
