#include "saver_image_by_date.h"

#include <QDir>
#include <QRegularExpression>
#include <QDate>
#include <QDirIterator>

SaverImageByDate::SaverImageByDate(const QString& image_directory, const char* image_format)
	: image_directory_(image_directory)
	, image_format_(image_format)
{
	if (!QDir(image_directory_).exists())
		QDir().mkdir(image_directory_);
}

QString SaverImageByDate::Write(const QImage& image)
{
	QString current_date_string = QDate::currentDate().toString("dd.MM.yyyy");
	QString image_name = current_date_string
								+ "_" + QString::number(CalculateImageName(current_date_string))
								+ "." + image_format_;
	QString full_saving_path = image_directory_ + "/" + image_name;

	if(image.save(full_saving_path))
		return image_name;
	return QString();
}

uint16_t SaverImageByDate::CalculateImageName(const QString& sDateName)
{
	QDirIterator image_iterator(QString(image_directory_),
					QStringList("*." + QString(image_format_)), QDir::Files);

	uint16_t max_number = 0;
	while (image_iterator.hasNext())
	{
		image_iterator.next();
		QRegularExpression regex(sDateName + "_(?<number>\\d+)" + "." + image_format_);

		QString image_name = image_iterator.fileName();
		QRegularExpressionMatch match = regex.match(image_name);
		if (match.hasMatch())
		{
			uint16_t temp_number = match.captured("number").toUShort();
			if (temp_number > max_number)
				max_number = temp_number;
		}
	}
	return max_number + 1;
}
