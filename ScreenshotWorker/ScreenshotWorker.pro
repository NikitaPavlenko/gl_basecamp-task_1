QT -= gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


HEADERS += \
    imaker.h \
    ireader.h \
    iworker.h \
    iwriter.h \
    maker_print_screen.h \
    reader_image_in_directory.h \
    saver_image_by_date.h \
    screenshot_worker.h

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

SOURCES += \
    maker_print_screen.cpp \
    reader_image_in_directory.cpp \
    saver_image_by_date.cpp
