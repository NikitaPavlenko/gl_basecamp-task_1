#pragma once

#include "iwriter.h"
#include <QImage>

class SaverImageByDate
		: public IWriter<QImage>
{
public:
	SaverImageByDate(const QString& image_directory, const char* image_format);

	QString Write(const QImage& image) override;
private:
	uint16_t CalculateImageName(const QString& date_string);

	QString image_directory_;
	const char* image_format_;
};
