#pragma once

#include "ireader.h"
#include <QImage>

class ReaderImageInDirectory
		: public IReader<QImage>
{
public:
	ReaderImageInDirectory(const QString& image_directory);

	QImage Read(const QString& image_name) override;

private:
	QString image_directory_;
};
