#pragma once
#include "imaker.h"
class QImage;

class MakerPrintScreen
		: public IMaker<QImage>
{
public:
	MakerPrintScreen();

	QImage Make() override;
};

