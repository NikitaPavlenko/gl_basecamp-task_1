#pragma once

template <typename T>
class IMaker
{
public:
	virtual ~IMaker() {}
	virtual T Make() = 0;
};

