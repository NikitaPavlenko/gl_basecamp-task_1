#pragma once

class QString;

template <typename T>
class IWriter
{
public:
	virtual ~IWriter() {}
	virtual QString Write(const T&) = 0;
};
