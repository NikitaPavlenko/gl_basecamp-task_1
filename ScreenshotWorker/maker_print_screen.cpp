#include "maker_print_screen.h"
#include <QGuiApplication>
#include <QScreen>
#include <QPixmap>

MakerPrintScreen::MakerPrintScreen() { }

QImage MakerPrintScreen::Make()
{
	QScreen* screen = QGuiApplication::primaryScreen();
	auto geom = screen->geometry();

	return screen->grabWindow(0, geom.x(), geom.y(), geom.width(), geom.height()).toImage();
}
